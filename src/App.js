import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    count: 6,
    value: [{}],
    title: '',
    isGoing: true,
  };

  incrementID = () => {
    this.setState((state, props) => ({
      count: state.count + 1,
    }));
  }

  handleChange = (event) => {
    event.persist()
    this.setState((state) => ({
      ...this.state,
      value: [{
        "userId": 1,
        "id": this.state.count,
        "title": event.target.value,
        "completed": false
      }],
      title: event.target.value
    }))
  }

  addToDo = (event) => {
    event.preventDefault()
    let newArray = this.state.todos.concat(this.state.value)
    this.setState((state) => ({
      ...this.state,
      count: state.count++,
      todos: newArray
    }))
    this.setState((state) => ({
      title: ''
    }))
  }

  handleToggleCheck = (event) => {
    const currentToDo = this.state.todos.filter(
      todo => todo.id === parseInt(event.target.id)
    )
    currentToDo.map(todo => {
      todo.completed = !todo.completed
      return todo
    })
    this.setState((state) => ({
      // todo DON'T KNOW HOW I WOULD SET STATE HEAR
    }))
  }

  destroyListItem = (event) => {
    const newToDosList = this.state.todos.filter(
      todo => todo.id !== parseInt(event.target.id)
    )
    this.setState((state) => ({
      todos: newToDosList
    }))
  }

  clearCompleted = () => {
    const completed = this.state.todos.filter(
      todo => !todo.completed
    )
    this.setState((state) => ({
      todos: completed
    }))
  }

  handleInputChange = (event) => {
    this.setState({
      [event.target.name]: event.target.name === 'isGoing' ? event.target.checked : event.target.value
    });
  }

  render() {
    const { title, todos } = this.state
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={this.addToDo}>
            <input
              className="new-todo"
              value={title}
              onChange={this.handleChange}
              placeholder="What needs to be done?"
              autoFocus
            />
          </form>
        </header>
        <TodoList
          todos={this.state.todos}
          onCheck={this.handleToggleCheck}
          destroy={this.destroyListItem}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>{todos.length}</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearCompleted}>Clear completed</button>
          <input type="checkbox" />
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  toggleCheckbox = (event) => {
    console.log(event.target.value)
  }

  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.onCheck}
            id={this.props.id}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.destroy} id={this.props.id}>x</button>
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              key={todo.id}
              id={todo.id}
              onCheck={this.props.onCheck}
              destroy={this.props.destroy}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
